/**
 * Test Units for the Validator Class
 * @author David Tran
 */

package movies.tests;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

import movies.importer.*;

class ValidatorTests {

	/**
	 * Unit Tests for validating the release year
	 */
	@Test
	public void releaseYearTestOne() {
		String source = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Source";
		String destination = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Destination";
		Validator newValidator = new Validator(source, destination);
		ArrayList<String> input = new ArrayList<String>();
		ArrayList<String> output = new ArrayList<String>();
		String lineOne = "2016\tYour Name\t120\tJapan";
		
		
		input.add(lineOne);
		output = newValidator.process(input);
		
		assertEquals("2016\tYour Name\t120\tJapan" , output.get(0));
	}
	
	@Test
	public void releaseYearTestTwo() {
		String source = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Source";
		String destination = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Destination";
		Validator newValidator = new Validator(source, destination);
		ArrayList<String> input = new ArrayList<String>();
		ArrayList<String> output = new ArrayList<String>();
		String lineOne = "hello	Your Name	120	Japan";
		
		input.add(lineOne);
		output = newValidator.process(input);
	
		assertTrue(output.isEmpty());
		
	}
	
	@Test
	public void releaseYearTestThree() {
		String source = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Source";
		String destination = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Destination";
		Validator newValidator = new Validator(source, destination);
		ArrayList<String> input = new ArrayList<String>();
		ArrayList<String> output = new ArrayList<String>();
		String lineOne = "null\tYour Name\t120\tJapan";
		
		input.add(lineOne);
		output = newValidator.process(input);
		
		assertTrue(output.isEmpty());

	}
	
	@Test
	public void releaseYearTestFour() {
		String source = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Source";
		String destination = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Destination";
		Validator newValidator = new Validator(source, destination);
		ArrayList<String> input = new ArrayList<String>();
		ArrayList<String> output = new ArrayList<String>();
		String lineOne = "\tYour Name\t120\tJapan";
		
		input.add(lineOne);
		output = newValidator.process(input);
		
		assertTrue(output.isEmpty());
	}
	
	
	
	/**
	 * Unit Tests for validating the movie title
	 */
	@Test
	public void movieTitleTestOne() {
		String source = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Source";
		String destination = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Destination";
		Validator newValidator = new Validator(source, destination);
		ArrayList<String> input = new ArrayList<String>();
		ArrayList<String> output = new ArrayList<String>();
		String lineOne = "2016\tYour Name\t120\tJapan";
		
		input.add(lineOne);
		output = newValidator.process(input);
		
		assertEquals("2016\tYour Name\t120\tJapan" , output.get(0));
	}
	
	@Test
	public void movieTitleTestTwo() {
		String source = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Source";
		String destination = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Destination";
		Validator newValidator = new Validator(source, destination);
		ArrayList<String> input = new ArrayList<String>();
		ArrayList<String> output = new ArrayList<String>();
		String lineOne = "2016\tnull\t120\tJapan";
		
		input.add(lineOne);
		output = newValidator.process(input);
		
		assertTrue(output.isEmpty());
	}
	
	@Test
	public void movieTitleTestThree() {
		String source = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Source";
		String destination = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Destination";
		Validator newValidator = new Validator(source, destination);
		ArrayList<String> input = new ArrayList<String>();
		ArrayList<String> output = new ArrayList<String>();
		String lineOne = "2016\t\t120\tJapan";
		
		input.add(lineOne);
		output = newValidator.process(input);
		
		assertTrue(output.isEmpty());
	}

	

	/**
	 * Unit Tests for validating the movie runtime
	 */
	@Test
	public void runtimeTestOne() {
		String source = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Source";
		String destination = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Destination";
		Validator newValidator = new Validator(source, destination);
		ArrayList<String> input = new ArrayList<String>();
		ArrayList<String> output = new ArrayList<String>();
		String lineOne = "2016\tYour Name\t120\tJapan";
		
		input.add(lineOne);
		output = newValidator.process(input);
		
		assertEquals("2016\tYour Name\t120\tJapan" , output.get(0));
	}
	
	@Test
	public void runtimeTestTwo() {
		String source = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Source";
		String destination = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Destination";
		Validator newValidator = new Validator(source, destination);
		ArrayList<String> input = new ArrayList<String>();
		ArrayList<String> output = new ArrayList<String>();
		String lineOne = "2016\tYour Name\thello\tJapan";
		
		input.add(lineOne);
		output = newValidator.process(input);
		
		assertTrue(output.isEmpty());
	}
	
	@Test
	public void runtimeTestThree() {
		String source = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Source";
		String destination = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Destination";
		Validator newValidator = new Validator(source, destination);
		ArrayList<String> input = new ArrayList<String>();
		ArrayList<String> output = new ArrayList<String>();
		String lineOne = "2016\tYour Name\tnull\tJapan";
		
		input.add(lineOne);
		output = newValidator.process(input);
		
		assertTrue(output.isEmpty());
	}
	
	@Test
	public void runtimeTestFour() {
		String source = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Source";
		String destination = "C:\\Users\\david\\OneDrive\\Desktop\\School\\Java\\Assignments\\Project_1\\Destination";
		Validator newValidator = new Validator(source, destination);
		ArrayList<String> input = new ArrayList<String>();
		ArrayList<String> output = new ArrayList<String>();
		String lineOne = "2016\tYour Name\t\tJapan";
		
		input.add(lineOne);
		output = newValidator.process(input);
		
		assertTrue(output.isEmpty());
	}
}