package movies.tests;
import movies.importer.*;
import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class MovieTest {
	/**
	 * Unit tests for the get methods
	 * @author Jackson Kwan and David Tran
	 */
	@Test
	public void getTests() {
		Movie testmovie = new Movie("2016", "Your Name", "6 months", "Japan");
		assertEquals("2016", testmovie.getReleaseYear());
		assertEquals("Your Name", testmovie.getMovieName());
		assertEquals("6 months", testmovie.getRuntime());
		assertEquals("Japan", testmovie.getSource());
	}
	
	/**
	 * Unit test for the toString overriding
	 * @author Jackson Kwan
	 */
	@Test
	public void toStringTest() {
		Movie testmovie = new Movie("2016", "Your Name", "120", "Japan");
		assertEquals("2016\tYour Name\t120\tJapan", testmovie.toString());
	}
	
	/**
	 * Unit test for the equals overriding
	 * @author David Tran
	 */
	@Test
	public void equalsTestOne() {
		Movie testmovieOne = new Movie("2016", "Your Name", "120", "Japan");
		Movie testmovieTwo = new Movie("2016", "Your Name", "125", "Japan");
		assertEquals(true, testmovieOne.equals(testmovieTwo));
	}
	
	@Test
	public void equalsTestTwo() {
		Movie testmovieOne = new Movie("2016", "Your Name", "120", "Japan");
		Movie testmovieTwo = new Movie("2016", "Your Name", "130", "Japan");
		assertEquals(false, testmovieOne.equals(testmovieTwo));
	}
	
	@Test
	public void equalsTestThree() {
		Movie testmovieOne = new Movie("1975", "the fortune", "90", "?");
		Movie testmovieTwo = new Movie("1975", "the fortune", "88", "?");
		assertEquals(true, testmovieOne.equals(testmovieTwo));
	}
}
