package movies.importer;

import java.util.ArrayList;

/**
 * ImdbImporter class that extends the Processor class for creating an Imdb Movie
 * @author David Tran
 *
 */

public class ImdbImporter extends Processor{
	public final int releaseYearIndex = 3;
	public final int movieTitleIndex = 1;
	public final int runTimeIndex = 6;
	public final String source = "Imdb";
	
	public ImdbImporter(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, true);
	}

	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> newImdb = new ArrayList<String>();
		
		for (int count = 0; count < input.size(); count++) {
			String[] movieSplitArray = input.get(count).split("\\t");
			String releaseYear = movieSplitArray[releaseYearIndex];
			String movieTitle = movieSplitArray[movieTitleIndex];
			String runTime = movieSplitArray[runTimeIndex];
			Movie newMovie = new Movie(releaseYear, movieTitle, runTime, source);
			newImdb.add(newMovie.toString());
		}
		
		return newImdb;
	}

}
