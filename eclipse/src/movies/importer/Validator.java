package movies.importer;

import java.util.ArrayList;

/**
 * Validator class that validates if the movies are actual movies (by checking if the givin data about the title, the release year and the runtime are correct)
 * @author David Tran
 *
 */

public class Validator extends Processor{

	public Validator(String sourceDir, String outputDir) {
		super(sourceDir, outputDir, false);
	}

	public ArrayList<String> process(ArrayList<String> input) {
		ArrayList<String> validatedMovie = new ArrayList<String>();
		String releaseYear = "";
		String movieTitle = "";
		String runTime = "";
		boolean validReleaseYear = true;
		boolean validMovieTitle = true;
		boolean validRunTime = true;
		
		for(int count = 0; count < input.size(); count++) {
			String[] movieString = input.get(count).split("\\t");
			releaseYear = movieString[0];
			movieTitle = movieString[1];
			runTime = movieString[2];
			
			
			
			// Validating the data of the movie part
			if(!(releaseYear.equals("null")) && !(releaseYear.isEmpty())) {
				try {
					Integer.parseInt(releaseYear);
				}
				
				catch(Exception e) {
					validReleaseYear = false;
				}
			}
			else {
				validReleaseYear = false;
			}
			
			
			if(!(movieTitle.equals("null")) && !(movieTitle.isEmpty())) {
				validMovieTitle = true;
			}
			else {
				validMovieTitle = false;
			}
			
			
			if(!(runTime.equals("null")) && !(runTime.isEmpty())) {
				try {
					Integer.parseInt(runTime);
				}
				
				catch(Exception e) {
					validRunTime = false;
				}
			}
			else {
				validRunTime = false;
			}		
			// End of validating the movie data part
			
			
			
			if(validReleaseYear && validMovieTitle && validRunTime) {
				validatedMovie.add(input.get(count));
			}			
			
		}
		
		return validatedMovie;
	}

}