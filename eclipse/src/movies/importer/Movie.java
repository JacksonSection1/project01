/**
 /* Movie class with get methods, constructors and toString overriding
 * @author Jackson Kwan and David Tran
 */
package movies.importer;

public class Movie {
	private String releaseYear;
	private String movieName;
	private String runtime;
	private String source;
	
	public Movie(String releaseYear, String movieName, String runtime, String source) {
		this.releaseYear = releaseYear;
		this.movieName = movieName;
		this.runtime = runtime;
		this.source = source;
	}

	public String getReleaseYear() {
		return this.releaseYear;
	}

	public String getMovieName() {
		return this.movieName;
	}

	public String getRuntime() {
		return this.runtime;
	}

	public String getSource() {
		return this.source;
	}
	
	public String toString() {
		return(this.releaseYear + "\t"+ this.movieName + "\t" + this.runtime + "\t" + this.source);
	}
	
	public void firstWord() {
		String[] tempArr = runtime.split(" ");
		runtime = tempArr[0];
	}
	
	public void lowered() {
		movieName = movieName.toLowerCase();
	}
	
	public boolean equals(Object o) {
		if (this.movieName.equals(((Movie)o).getMovieName())  && this.releaseYear.equals((((Movie)o).getReleaseYear()))) {
			int difference = Integer.parseInt(((Movie)o).getRuntime()) - Integer.parseInt(this.runtime);
			if (difference <= 5 && difference >= -5) {
				return true;
			}
		}
		return false;
	}
}
